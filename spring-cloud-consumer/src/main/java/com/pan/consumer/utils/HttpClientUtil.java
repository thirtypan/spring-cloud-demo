package com.pan.consumer.utils;



import dto.CommonsRes;
import lombok.extern.slf4j.Slf4j;

import org.apache.hc.client5.http.HttpRoute;
import org.apache.hc.client5.http.async.methods.SimpleHttpRequest;
import org.apache.hc.client5.http.async.methods.SimpleHttpResponse;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.client5.http.impl.async.HttpAsyncClients;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManager;
import org.apache.hc.client5.http.nio.AsyncClientConnectionManager;
import org.apache.hc.client5.http.nio.AsyncConnectionEndpoint;
import org.apache.hc.client5.http.socket.ConnectionSocketFactory;
import org.apache.hc.client5.http.socket.PlainConnectionSocketFactory;
import org.apache.hc.core5.concurrent.FutureCallback;
import org.apache.hc.core5.http.ClassicHttpRequest;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.io.SocketConfig;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.apache.hc.core5.http.io.support.ClassicRequestBuilder;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.apache.hc.core5.http.protocol.HttpContext;
import org.apache.hc.core5.io.CloseMode;
import org.apache.hc.core5.reactor.ConnectionInitiator;
import org.apache.hc.core5.util.TimeValue;
import org.apache.hc.core5.util.Timeout;


import java.io.*;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * &#064;desc  httpClient工具类
 * @author liangliang
 * &#064;date  2019/3/11 13:23
 */

@Slf4j
public class HttpClientUtil {
    public static final String XML_TYPE = "xml";
    public static final String JSON_TYPE = "json";




    /**初始化
     *
     */
    public static void init(){
        //  整个连接池连接的最大值
        manager.setMaxTotal(200);
        // 每路由最大连接数，默认值是2
        manager.setDefaultMaxPerRoute(100);
        //socket配置
        manager.setDefaultSocketConfig(socketConfig);


    }
    /**
     * socket基础配置信息
     */
    private static final SocketConfig socketConfig = SocketConfig.custom()
            .setSoTimeout(30000, TimeUnit.MILLISECONDS)
            .setTcpNoDelay(true)
            .build();

    /**
     * httpclient基础配置信息
     */
    private static final RequestConfig requestConfig = RequestConfig.custom()
            // 设置请求超时时间(单位毫秒)
            .setConnectionRequestTimeout(30000, TimeUnit.MILLISECONDS)
            // 设置返回超时时间(单位毫秒)
            .setResponseTimeout(30000, TimeUnit.MILLISECONDS)
            // 设置是否允许重定向(默认为true)
            .setRedirectsEnabled(true)
            //是否启用内容压缩，默认true
            .setContentCompressionEnabled(true)
            .build();
    /**
     * 连接池管理类
     */
    private static final  PoolingHttpClientConnectionManager manager = new PoolingHttpClientConnectionManager();
    private static final
    AsyncClientConnectionManager asyncManager = new AsyncClientConnectionManager() {
        @Override
        public void close() throws IOException {

        }
        @Override
        public void close(CloseMode closeMode) {

        }
        @Override
        public Future<AsyncConnectionEndpoint> lease(String id, HttpRoute route, Object state, Timeout requestTimeout, FutureCallback<AsyncConnectionEndpoint> callback) {
            return null;
        }
        @Override
        public void release(AsyncConnectionEndpoint endpoint, Object newState, TimeValue validDuration) {

        }
        @Override
        public Future<AsyncConnectionEndpoint> connect(AsyncConnectionEndpoint endpoint, ConnectionInitiator connectionInitiator, Timeout connectTimeout, Object attachment, HttpContext context, FutureCallback<AsyncConnectionEndpoint> callback) {
            return null;
        }
        @Override
        public void upgrade(AsyncConnectionEndpoint endpoint, Object attachment, HttpContext context) {

        }
    };

    /**
     * 获得Http客户端
     */
    public static final CloseableHttpClient HTTP_CLIENT = HttpClientBuilder.create()
            .setConnectionManager(manager)
            .setDefaultRequestConfig(requestConfig)
            .build();
    /**
     * 异步Http客户端
     */
    private static final CloseableHttpAsyncClient HTTP_ASYNC_CLIENT = HttpAsyncClients.custom()
            .setConnectionManager(asyncManager)
            .setDefaultRequestConfig(requestConfig)
            .build();


    /**
     * @desc 异步请求
     * @param request SimpleHttpRequest
     * @author liangliang
     * @date 2019/3/11 13:23
     */
    private static void executeAsync(SimpleHttpRequest request) {
        try(CloseableHttpAsyncClient httpClient = HttpAsyncClients.createDefault()){
            HTTP_ASYNC_CLIENT.start();
            httpClient.start();
            httpClient.execute(request, new FutureCallback<SimpleHttpResponse>() {
                @Override
                public void completed(SimpleHttpResponse response) {
                    log.info("请求完成，状态码为-> {}", response.getCode());
                }
                @Override
                public void failed(Exception e) {
                    log.info("请求失败-> {}", e.getMessage());
                }
                @Override
                public void cancelled() {
                    log.info("请求取消-> cancelled");
                }
            });
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    /**
     * get请求
     *
     * @param url
     * @return
     */
    public static CommonsRes<String> get(String url,Map<String, String> headers) {
        try  {
            ClassicHttpRequest httpGet = ClassicRequestBuilder.get(url).build();
            getHeaders(headers,httpGet);
            return HTTP_CLIENT.execute(httpGet, response -> new CommonsRes<>(response.getCode(),"suc",EntityUtils.toString(response.getEntity())));
        } catch (IOException e) {
            return new CommonsRes<>(9999,"error",e.getMessage());
        }
    }
    /**
     * Post 请求
     *
     * @param url
     * @return
     */
    public static CommonsRes<String> Post(String url,Map<String, Object> params,Map<String, String> headers) {
        try  {
            ClassicHttpRequest httpPost = ClassicRequestBuilder.post(url).build();
            getHeaders(headers,httpPost);
            getParamsForMap(params,httpPost);
            return HTTP_CLIENT.execute(httpPost, response -> new CommonsRes<>(response.getCode(),"suc",EntityUtils.toString(response.getEntity())));
        } catch (IOException e) {
            return new CommonsRes<>(9999,"error",e.getMessage());
        }
    }
    /**
     * Post 请求
     *
     * @param url 路径
     * @param type 类型 xml、json
     * @return
     */
    public static CommonsRes<String> Post(String url,String params,Map<String, String> headers,String type) {
        try  {
            ClassicHttpRequest httpPost = ClassicRequestBuilder.post(url).build();
            getHeaders(headers,httpPost);
            switch (type){
                case XML_TYPE:getParamsForXml(params,httpPost);break;
                case JSON_TYPE:getParamsForJson(params,httpPost);break;
                default:
            }
            return HTTP_CLIENT.execute(httpPost, response -> new CommonsRes<>(response.getCode(),"suc",EntityUtils.toString(response.getEntity())));
        } catch (IOException e) {
            return new CommonsRes<>(9999,"error",e.getMessage());
        }
    }

    /**
     * 封装请求参数（Map）
     * @param params
     */
    private static void getParamsForMap(Map<String, Object> params,ClassicHttpRequest httpRequest){
        if (params!=null){
            List<NameValuePair> pairs = new ArrayList<>();
            for (Map.Entry<String, Object> param : params.entrySet()) {
                pairs.add(new BasicNameValuePair(param.getKey(), String.valueOf(param.getValue())));
            }
            httpRequest.setEntity(new UrlEncodedFormEntity(pairs));
        }
    }
    /**
     * 封装请求参数（Json）
     * @param params
     */
    private static void getParamsForJson(String params,ClassicHttpRequest httpRequest){
        httpRequest.addHeader("Content-Type", "application/json;charset=UTF-8");
        if (params!=null){
            httpRequest.setEntity(new StringEntity(params, StandardCharsets.UTF_8));
        }
    }
    /**
     * 封装请求参数（Xml）
     * @param params
     */
    private static void getParamsForXml(String params,ClassicHttpRequest httpRequest){
        httpRequest.addHeader("Content-Type", "application/xml;charset=UTF-8");
        if (params!=null){
            httpRequest.setEntity(new StringEntity(params, StandardCharsets.UTF_8));
        }
    }
    /**
     * 封装请求头
     *
     * @param headers
     * @param httpRequest
     */
    private static void getHeaders(Map<String, String> headers,ClassicHttpRequest httpRequest){
        // 封装请求头
        if (headers != null) {
            Set<Map.Entry<String, String>> entrySet = headers.entrySet();
            for (Map.Entry<String, String> entry : entrySet) {
                // 设置到请求头到HttpRequestBase对象中
                httpRequest.setHeader(entry.getKey(), entry.getValue());
            }
        }
    }


}


