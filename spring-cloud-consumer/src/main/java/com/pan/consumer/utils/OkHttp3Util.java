package com.pan.consumer.utils;

import dto.CommonsRes;
import okhttp3.*;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.core5.http.ClassicHttpRequest;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.apache.hc.core5.http.message.BasicNameValuePair;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class OkHttp3Util {


    public static final String JSON_TYPE =  "json";
    public static final MediaType URL_ENCODED = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");
    public static final MediaType FORM_DATA = MediaType.parse("multipart/form-data; charset=utf-8");
    public static final String XML_TYPE = "xml";

    /**
     * 初始化
     */
    public static void init() {

    }

    /**
     * oKhTTP客户段获取
     */
    private static final OkHttpClient okHttpClient = new OkHttpClient.Builder()
            //设置连接超时时间
            .connectTimeout(10, TimeUnit.SECONDS)
            //设置读取超时时间
            .readTimeout(20, TimeUnit.SECONDS)
            .build();;
    /**
     * get请求
     *
     * @param url
     * @param headerMap
     * @return
     * @throws Exception
     */
    public static CommonsRes<String> get(String url, Map<String, String> headerMap)  {
        try{
            Request.Builder builder = new Request.Builder().url(url);
            getHeaders(headerMap,builder);
            Request request = builder.build();
            Response response  = okHttpClient.newCall(request).execute();
            if (response.body() != null) {
                return new CommonsRes<>(response.code(),"suc",response.body().string());
            }else {
                return new CommonsRes<>(response.code(),"suc","");
            }
        }catch (Exception e){
            return new CommonsRes<>(9999,"error",e.getMessage());
        }
    }
    /**
     * post 请求 :map
     *
     * @param url
     * @param headerMap
     * @return
     * @throws Exception
     */
    public static CommonsRes<String> post(String url, Map<String, String> headerMap,Map<String,Object> params)  {
        try{

            Request.Builder builder = new Request.Builder().post(getParamsForMap(params)).url(url);
            getHeaders(headerMap,builder);
            Request request = builder.build();
            Response response  = okHttpClient.newCall(request).execute();
            if (response.body() != null) {
                return new CommonsRes<>(response.code(),"suc",response.body().string());
            }else {
                return new CommonsRes<>(response.code(),"suc","");
            }
        }catch (Exception e){
            return new CommonsRes<>(9999,"error",e.getMessage());
        }
    }
    /**
     * post 请求 :json
     *
     * @param url
     * @param headerMap
     * @return
     * @throws Exception
     */
    public static CommonsRes<String> post(String url, Map<String, String> headerMap,String params,String type)  {
        try{
            RequestBody requestBody;
            switch (type){
                case JSON_TYPE:requestBody = getParamsForJson(params);break;
                case XML_TYPE:requestBody = getParamsForXml(params);break;
                default:
                   throw new RuntimeException("必须指定请求类型");

            }

            Request.Builder builder = new Request.Builder().post(requestBody).url(url);
            getHeaders(headerMap,builder);
            Request request = builder.build();
            Response response  = okHttpClient.newCall(request).execute();
            if (response.body() != null) {
                return new CommonsRes<>(response.code(),"suc",response.body().string());
            }else {
                return new CommonsRes<>(response.code(),"suc","");
            }
        }catch (Exception e){
            return new CommonsRes<>(9999,"error",e.getMessage());
        }
    }


    /**
     * 封装请求头
     *
     * @param headers
     * @param builder
     */
    private static void getHeaders(Map<String, String> headers, Request.Builder builder){
        // 封装请求头
        if (headers != null) {
            if (!headers.isEmpty()) {
                headers.forEach(builder::addHeader);
            }
        }
    }

    /**
     * 封装请求参数（Map）
     * @param params
     */
    private static FormBody getParamsForMap(Map<String, Object> params){
        FormBody.Builder builder  = new FormBody.Builder();
        if (params!=null){
            for (Map.Entry<String, Object> param : params.entrySet()) {
                builder.add(param.getKey(), String.valueOf(param.getValue()));
            }
        }
        return builder.build();
    }
    /**
     * 封装请求参数（Json）
     * @param params
     */
    private static RequestBody getParamsForJson(String params){
        MediaType mediaType=MediaType.Companion.parse("application/json;charset=utf-8");
        return RequestBody.Companion.create(params,mediaType);
    }
    /**
     * 封装请求参数（Xml）
     * @param params
     */
    private static RequestBody getParamsForXml(String params){
        MediaType mediaType=MediaType.Companion.parse("application/xml;charset=utf-8");
        return RequestBody.Companion.create(params,mediaType);
    }


}
