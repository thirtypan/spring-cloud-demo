package com.pan.consumer;

import com.pan.consumer.config.RibbonConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;


@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
//排除Ribben自定义负载算法
//@ComponentScan(excludeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {RibbonConfig.class})})
//@RibbonClient(name = "springCloudProducer",configuration = RibbonConfig.class)
//ribbon被移除，负载均衡推荐的loadbalance替换ribbon
@LoadBalancerClients({
        @LoadBalancerClient(value = "springCloudProducer", configuration = RibbonConfig.class),
})



public class SpringCloudConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudConsumerApplication.class, args);
    }

}
