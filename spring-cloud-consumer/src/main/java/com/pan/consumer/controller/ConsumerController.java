package com.pan.consumer.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.pan.consumer.config.SentinelBlockHandle;
import com.pan.consumer.config.SentinelFallback;
import dto.CommonsRes;
import entity.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@Slf4j
public class ConsumerController {
    //调用支付订单服务端的ip+端口号
    private static final  String PRODUCER_URL = "http://springCloudProducer";
    private final RestTemplate restTemplate;
    @Autowired
    public ConsumerController(RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }

    //创建支付订单的接口
    @GetMapping("/consumer/savePerson")
    // 限流与阻塞处理 blockHandler
    // 熔断与降级处理 fallback
    @SentinelResource(value = "savePerson",
            fallback  = "fallbackException",
            fallbackClass = SentinelFallback.class,
            blockHandler = "handleException",
            blockHandlerClass = SentinelBlockHandle.class)
    public CommonsRes<Person> create(){
        Person person =new Person("","测试",18,0,"19915300382","");
        ResponseEntity<CommonsRes> responseEntity = restTemplate.postForEntity(PRODUCER_URL + "/producer/savePerson", person, CommonsRes.class);
        return responseEntity.getBody();
    }

}
