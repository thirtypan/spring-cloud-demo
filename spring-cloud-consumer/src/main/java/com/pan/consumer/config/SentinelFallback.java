package com.pan.consumer.config;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import dto.CommonsRes;

public class SentinelFallback {

    /**
     * sentinel熔断降级处理逻辑
     *
     * @param exception
     * @return
     */
    public CommonsRes<String> fallbackException(BlockException exception){
        return new  CommonsRes<String> (200,"服务降级");
    }
}
