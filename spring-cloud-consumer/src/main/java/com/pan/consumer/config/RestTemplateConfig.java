package com.pan.consumer.config;


import com.alibaba.cloud.sentinel.annotation.SentinelRestTemplate;
import com.pan.consumer.utils.HttpClientUtil;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {





    /**
     * 请求连接池配置
     * @param
     * @return
     */
    @Bean
    public ClientHttpRequestFactory clientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        // httpClient创建器
        clientHttpRequestFactory.setHttpClient(HttpClientUtil.HTTP_CLIENT);
        return clientHttpRequestFactory;
    }

    /**
     * rest模板
     * @return
     */
    @Bean
    @LoadBalanced //开启负载(默认轮询)
    @SentinelRestTemplate
    public RestTemplate restTemplate(ClientHttpRequestFactory clientHttpRequestFactory) {
        // boot中可使用RestTemplateBuilder.build创建
        RestTemplate restTemplate = new RestTemplateBuilder().build();
        // 配置请求工厂
        restTemplate.setRequestFactory(clientHttpRequestFactory);
        return restTemplate;
    }

}
