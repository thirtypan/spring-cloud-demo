package com.pan.producer;



import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@MapperScan("com.pan.producer.mapper")
public class SpringCloudProducerApplication {

    public static void main(String[] args) {

        SpringApplication.run(SpringCloudProducerApplication.class, args);
    }

}
