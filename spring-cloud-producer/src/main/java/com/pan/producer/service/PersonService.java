package com.pan.producer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import entity.Person;

public interface PersonService extends IService<Person> {
}
