package com.pan.producer.controller;

import com.alibaba.fastjson2.JSON;
import com.pan.producer.mapper.PersonMapper;
import com.pan.producer.service.PersonService;
import com.pan.producer.utils.RedisUtil;
import dto.CommonsRes;
import entity.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/producer")
public class PersonController  {

    private final PersonService personService;

    private  PersonMapper personMapper;

    private RedisUtil redisUtil;

    /**
     * 构造器注入
     * @param personService
     */
    @Autowired
    public PersonController(PersonService personService){
        this.personService = personService;
    }

    /**
     * set注入
     * @param personMapper
     */
    @Autowired
    public void setPersonMapper(PersonMapper personMapper){
        this.personMapper = personMapper;
    }
    @Autowired
    public void setRedisUtil(RedisUtil redisUtil){
        this.redisUtil = redisUtil;
    }
    @PostMapping("/savePerson")
    public CommonsRes<Person> savePerson(@RequestBody Person person){
        try{
            personService.save(person);
//            Thread.sleep(6000);
            return new CommonsRes<>(200,"请求成功");
        }catch (Exception e){
            log.error("保存失败:"+e.getMessage());
            return new CommonsRes<>(500,"技术错误");
        }
    }
    @PostMapping("/redisSavePerson")
    public CommonsRes<Person> redisSaveString(){
        try{
            Person person =new Person("","测试",18,0,"19915300382","");
            redisUtil.set("person", JSON.toJSONString(person));
            String person1 = redisUtil.get("person").toString();
            return new CommonsRes<>(200,"请求成功");
        }catch (Exception e){
            log.error("保存失败:"+e.getMessage());
            return new CommonsRes<>(500,"技术错误");
        }
    }
}
