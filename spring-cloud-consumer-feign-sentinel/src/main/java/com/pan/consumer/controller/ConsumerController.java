package com.pan.consumer.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.pan.consumer.config.SentinelBlockHandle;
import com.pan.consumer.service.PersonService;
import dto.CommonsRes;
import entity.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Slf4j
public class ConsumerController {

    private final PersonService personService;
    @Autowired
    public ConsumerController(PersonService personService){
        this.personService = personService;
    }

    //创建支付订单的接口
    @GetMapping("/consumer/savePerson")
    public CommonsRes<Person> create(){
        Person person =new Person("","测试",18,0,"19915300382","");
        return personService.savePerson(person);
    }


}
