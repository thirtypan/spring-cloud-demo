package com.pan.consumer.config;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import dto.CommonsRes;

public class SentinelBlockHandle {
    /**
     * sentinel限流处理逻辑 byurl
     *
     * @param exception
     * @return
     */
    public CommonsRes<String> handleExceptionByUrl(BlockException exception){
        return new  CommonsRes<String> (200,exception.getClass().getCanonicalName());
    }
}
