package com.pan.consumer.service;


import com.pan.consumer.service.fallback.PersonFallbackService;
import dto.CommonsRes;
import entity.Person;
import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
//当发生http 404错误时，如果dismiss404为true，会调用decoder进行解码，否则抛出FeignException
@FeignClient(value ="springCloudProducer",dismiss404 = true,fallback = PersonFallbackService.class)
public interface PersonService  {
//    @PostMapping("/producer/savePerson")
    @RequestLine("POST /producer/savePerson")
    CommonsRes<Person> savePerson(@RequestBody Person person);
}
