package com.pan.consumer.service.fallback;

import com.pan.consumer.service.PersonService;
import dto.CommonsRes;
import entity.Person;
import org.springframework.stereotype.Component;

@Component
public class PersonFallbackService implements PersonService {
    @Override
    public CommonsRes<Person> savePerson(Person person) {
        return new CommonsRes<>(200,"服务降级返回");
    }
}
