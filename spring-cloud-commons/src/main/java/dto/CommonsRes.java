package dto;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * description:公共返回类
 *
 * @author pan
 * @version 1.0
 * @date 17:59
 */

@Getter
@Setter
/*无参构造*/
@NoArgsConstructor
/*全参构造*/
@AllArgsConstructor
public class CommonsRes<T> {
    /**返回状态码*/
    private Integer code;
    /**返回是否调用成功*/
    private String  message;
    /**返回的数据*/
    private T data;

    public CommonsRes(Integer code, String message) {
        this(code,message,null);
    }

}
