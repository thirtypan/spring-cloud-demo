package entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
/*无参构造*/
@NoArgsConstructor
/*全参构造*/
@AllArgsConstructor
/* getter 和 setter 三个功能 */
@Accessors(chain = true)
@TableName("person")
public class Person implements Serializable {
    /**名字*/
    @TableId
    @TableField("person_id")
    private String personId;
    /**名字*/
    private String name;
    /**年龄*/
    private int age;
    /**性别*/
    private int sex;
    /**手机号*/
    private String phone;
    /**住址*/
    private String address;
}
