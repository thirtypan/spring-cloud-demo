package com.pan.nacosProducer.controller;


import com.pan.nacosProducer.mapper.PersonMapper;
import com.pan.nacosProducer.service.PersonService;
import dto.CommonsRes;
import entity.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/producer")
public class PersonController  {

    private final PersonService personService;

    private PersonMapper personMapper;

    /**
     * 构造器注入
     * @param personService
     */
    @Autowired
    public PersonController(PersonService personService){
        this.personService = personService;
    }

    /**
     * set注入
     * @param personMapper
     */
    @Autowired
    public void setPersonMapper(PersonMapper personMapper){
        this.personMapper = personMapper;
    }
    @PostMapping("/savePerson")
    public CommonsRes<Person> savePerson(@RequestBody Person person){
        try{
            personService.save(person);
//            Thread.sleep(6000);
            return new CommonsRes<>(200,"请求成功");
        }catch (Exception e){
            log.error("保存失败:"+e.getMessage());
            return new CommonsRes<>(500,"技术错误");
        }
    }
}
