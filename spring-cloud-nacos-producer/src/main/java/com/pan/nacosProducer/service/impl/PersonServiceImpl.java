package com.pan.nacosProducer.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.pan.nacosProducer.mapper.PersonMapper;
import com.pan.nacosProducer.service.PersonService;
import entity.Person;
import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl extends ServiceImpl<PersonMapper, Person> implements PersonService {
}
