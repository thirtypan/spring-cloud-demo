package com.pan.nacosProducer;



import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


@SpringBootApplication
@MapperScan("com.pan.nacosProducer.mapper")
public class SpringCloudProducerNacosApplication {

    public static void main(String[] args) {

        ConfigurableApplicationContext run = SpringApplication.run(SpringCloudProducerNacosApplication.class, args);
        String userName = run.getEnvironment().getProperty("nacosTestConfig.name");
        System.out.println(userName);
    }

}
